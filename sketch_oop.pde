Ball ball1;
Ball ball2;
Ball ball3;
Ball ball4; 

void setup() {
  size(700,700);
  ball1 = new Ball(0,1,1,20);
  ball2 = new Ball(0,2,2,20);
  ball3= new Ball(0,3,3,20);
  ball4 = new Ball(0,4,4,20);
}

void draw() {
  ball1.display();
  ball1.drive();
  ball2.display();
  ball2.drive();
  ball3.display();
  ball3.drive();
  ball4.display();
  ball4.drive();
}

class Ball {
  
  float x;
  float h;
  float d;
  float s;

  
  Ball(float start, float heightt, float speed, float diameter) {
   
    x= start;
    h = heightt * (700/5);
    s = speed;
    d = diameter;
  }

  void display() {
    ellipse(x, h, d, d);
  }

  void drive() {
    x = x +s;
    
  }
}
